Wireshark Ambit Protocol Dissector
==================================

This project provides a [Wireshark][1] packet dissector to help
reverse engineer the device protocol used by (many of) the sport
swatches in the Suunto Ambit family.  The dissector picks apart
USB packet captures marking not only the known bits but also the
parts that are still not (well) understood.

All code, documentation and supporting files part of this project are
provided under the terms of the [GNU General Public License, version
3][2] or later.

 [1]: https://www.wireshark.org/
 [2]: https://gitlab.com/ambitious/dissector/blob/master/LICENSE.md

Build Requirements
------------------

To build the dissector, you need:

 - [CMake][3],
 - a standards compliant C compiler, e.g. [gcc][4] or [clang][5], and
 - the Wireshark development libraries and tools

The latter may require extra development libraries.

A [Dockerfile][6] to create an image that meets the build requirements
is part of the project.

 [3]: https://cmake.org/
 [4]: https://gcc.gnu.org/
 [5]: https://clang.llvm.org/
 [6]: https://gitlab.com/ambitious/dissector/blob/master/Dockerfile

Build And Installation Procedure
--------------------------------

CMake projects normally build in a directory separate from the source.
That goes something like this

``` sh
mkdir -p _build
cd _build
cmake ..
make
```

At present there are no build-time options (other than those provided
by CMake itself).

Installing the dissector below your `$HOME` directory is achieved with

``` sh
make install
```

When using the Docker image for build purposes, please note that the
installation needs to be done *outside* of the container.
