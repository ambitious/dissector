# Dockerfile -- for the dissector development environment
# Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM registry.gitlab.com/paddy-hack/devuan/slim:ascii

RUN apt-get -qq update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get -qq install --assume-yes \
            clang clang-format \
            cmake \
            wireshark-dev
