// moves-count.c -- a tally of moves on your watch
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ambit.h"

#include <epan/expert.h>

static int proto_ambit_moves_count = -1;

static int hf_ambit_moves_count = -1;

extern expert_field ei_ambit_message_payload_undecoded;

static int
dissect_ambit_moves_count_reply(tvbuff_t* tvb, packet_info* pinfo,
                                proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  proto_tree_add_expert(tree, pinfo, &ei_ambit_message_payload_undecoded, tvb,
                        offset, 2);
  offset += 2;
  ADD_ITEM(hf_ambit_moves_count, 2, ENC_LITTLE_ENDIAN);
  return offset;
}

static int
dissect_ambit_moves_count(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree,
                          void* data)
{
  ambit_message* message = (ambit_message*)data;

  return ((message->flags & 0x01) ? 0 : dissect_ambit_moves_count_reply(
                                          tvb, pinfo, tree, data));
}

void
proto_register___ambit_moves_count(void)
{
  static hf_register_info hf[] = { { &hf_ambit_moves_count,
                                     { "Moves", "ambit.moves.count", FT_UINT16,
                                       BASE_DEC, NULL, 0x0, NULL, HFILL } } };

  proto_ambit_moves_count = proto_register_protocol(
    "Get Moves Count", "Get Moves Count", "ambit.moves");

  proto_register_field_array(proto_ambit_moves_count, hf, array_length(hf));
}

void
proto_reg_handoff___ambit_moves_count(void)
{
  static dissector_handle_t ambit_moves_count_handle;

  ambit_moves_count_handle =
    create_dissector_handle(dissect_ambit_moves_count, proto_ambit_moves_count);
  dissector_add_uint("ambit.message.code", 0x0b06, ambit_moves_count_handle);
}
