// ambit.h -- protocol constants and shared functionality
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <epan/packet.h>

#define AMBIT_PACKET_MARKER (0x3f)
#define AMBIT_STARTER_PACKET (0x5d)
#define AMBIT_TRAILER_PACKET (0x5e)

typedef struct
{
  guint type;
  guint size;
  guint index;
} ambit_packet;

typedef struct
{
  guint flags;
  guint status;
} ambit_message;

int proto_ambit_packet;
int proto_ambit_message;

proto_item* ambit_tree_add_date(proto_tree* tree, int hfindex, tvbuff_t* tvb,
                                const gint start, gint length);

proto_item* ambit_tree_add_time(proto_tree* tree, int hfindex, tvbuff_t* tvb,
                                const gint start, gint length);

proto_item* ambit_tree_add_version(proto_tree* tree, int hfindex, tvbuff_t* tvb,
                                   const gint start, gint length);

#define ADD_ITEM(hf_index, length, encoding)                                   \
  proto_tree_add_item(tree, hf_index, tvb, offset, length, encoding);          \
  offset += length
