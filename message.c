// message.c -- header dissection and payload dissector hand-off
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <epan/packet.h>
#include <epan/reassemble.h>
#include <epan/unit_strings.h>

#include <epan/expert.h>

#include "ambit.h"

int proto_ambit_message = -1;

static int hf_ambit_message_code = -1;
static int hf_ambit_message_flags = -1;
static int hf_ambit_message_status = -1;
static int hf_ambit_message_format = -1;
static int hf_ambit_message_counter = -1;
static int hf_ambit_message_size = -1;
static int hf_ambit_message_payload = -1;

static int ett_ambit_message = -1;

static int hf_ambit_message_fragments = -1;
static int hf_ambit_message_fragment = -1;
static int hf_ambit_message_fragment_overlap = -1;
static int hf_ambit_message_fragment_overlap_conflicts = -1;
static int hf_ambit_message_fragment_multiple_tails = -1;
static int hf_ambit_message_fragment_too_long_fragment = -1;
static int hf_ambit_message_fragment_error = -1;
static int hf_ambit_message_fragment_count = -1;
static int hf_ambit_message_reassembled_in = -1;
static int hf_ambit_message_reassembled_length = -1;
static int hf_ambit_message_reassembled_data = -1;

static gint ett_ambit_message_fragment = -1;
static gint ett_ambit_message_fragments = -1;

static dissector_table_t ambit_payload_dissector_table;

static reassembly_table ambit_message_reassembly_table;

static const fragment_items ambit_message_fragment_items = {
  &ett_ambit_message_fragment,
  &ett_ambit_message_fragments,
  &hf_ambit_message_fragments,
  &hf_ambit_message_fragment,
  &hf_ambit_message_fragment_overlap,
  &hf_ambit_message_fragment_overlap_conflicts,
  &hf_ambit_message_fragment_multiple_tails,
  &hf_ambit_message_fragment_too_long_fragment,
  &hf_ambit_message_fragment_error,
  &hf_ambit_message_fragment_count,
  &hf_ambit_message_reassembled_in,
  &hf_ambit_message_reassembled_length,
  &hf_ambit_message_reassembled_data,
  "Message fragments"
};

static expert_field ei_ambit_message_undecoded = EI_INIT;
expert_field ei_ambit_message_payload_undecoded = EI_INIT;

static const value_string ambit_message_code_names[] = {
  { 0x0000, "Handshake" },
  { 0x0002, "Handshake" },
  { 0x0300, "Set DateTime" },
  { 0x0302, "Set Date" },
  { 0x0306, "Get Device Status" },
  { 0x0700, "Get FAT16 Block" },
  { 0x0b06, "Get Moves Count" },
  { 0x0b17, "Get BBPMEM.DAT Block" },
  { 0, NULL }
};

static int
dissect_ambit_message(tvbuff_t* _tvb, packet_info* pinfo, proto_tree* _tree,
                      void* data)
{
  col_clear(pinfo->cinfo, COL_INFO);

  proto_item* tree_item =
    proto_tree_add_item(_tree, proto_ambit_message, _tvb, 0, -1, ENC_NA);
  proto_tree* tree = proto_item_add_subtree(tree_item, ett_ambit_message);

  ambit_packet* packet = (ambit_packet*)data;

  tvbuff_t* tvb = NULL;
  if (packet->type == AMBIT_TRAILER_PACKET || packet->index > 1) {
    guint current = (packet->type == AMBIT_TRAILER_PACKET ? packet->index : 0);

    if (packet->type == AMBIT_STARTER_PACKET)
      fragment_start_seq_check(&ambit_message_reassembly_table, pinfo, 0, NULL,
                               packet->index - 1);

    fragment_head* head =
      fragment_add_seq_check(&ambit_message_reassembly_table, _tvb, 0, pinfo, 0,
                             NULL, current, packet->size, TRUE);

    tvb = process_reassembled_data(_tvb, 0, pinfo, "Reassembled message", head,
                                   &ambit_message_fragment_items, NULL, tree);

    if (!tvb) {
      col_append_str(pinfo->cinfo, COL_INFO, "(message fragment)");
      return 0;
    }
  } else if (packet->type == AMBIT_STARTER_PACKET && packet->size > 0) {
    tvb = tvb_new_subset_length(_tvb, 0, packet->size);
  } else {
    return 0;
  }

  guint16 code = tvb_get_guint16(tvb, 0, ENC_BIG_ENDIAN);
  guint8 flags = tvb_get_guint8(tvb, 2);

  col_append_str(
    pinfo->cinfo, COL_INFO,
    val_to_str(code, ambit_message_code_names, "Unknown [0x%04x]"));
  col_append_str(pinfo->cinfo, COL_INFO,
                 (0x01 & flags) ? " (request)" : " (response)");

  ambit_message message;

  gint offset = 0;
  ADD_ITEM(hf_ambit_message_code, 2, ENC_NA);
  message.flags = tvb_get_guint8(tvb, offset);
  ADD_ITEM(hf_ambit_message_flags, 1, ENC_NA);
  message.status = tvb_get_guint8(tvb, offset);
  ADD_ITEM(hf_ambit_message_status, 1, ENC_NA);
  ADD_ITEM(hf_ambit_message_format, 2, ENC_NA);
  ADD_ITEM(hf_ambit_message_counter, 2, ENC_LITTLE_ENDIAN);
  guint32 size = tvb_get_guint32(tvb, offset, ENC_LITTLE_ENDIAN);
  ADD_ITEM(hf_ambit_message_size, 4, ENC_LITTLE_ENDIAN);

  if (size > 0) {
    tvbuff_t* next_tvb = tvb_new_subset_remaining(tvb, offset);

    proto_item* payload_item = ADD_ITEM(hf_ambit_message_payload, size, ENC_NA);
    proto_tree* payload_tree =
      proto_item_add_subtree(payload_item, ett_ambit_message);

    guint dissected =
      dissector_try_uint_new(ambit_payload_dissector_table, code, next_tvb,
                             pinfo, payload_tree, FALSE, &message);
    if (dissected < tvb_captured_length(next_tvb)) {
      proto_tree_add_expert(payload_tree, pinfo,
                            &ei_ambit_message_payload_undecoded, next_tvb,
                            dissected, -1);
    }
  }

  if (offset < tvb_captured_length(tvb)) {
    proto_tree_add_expert(tree, pinfo, &ei_ambit_message_undecoded, tvb, offset,
                          -1);
  }

  return tvb_captured_length(tvb);
}

void
proto_register__ambit_message(void)
{
  static hf_register_info hf[] = {
    { &hf_ambit_message_code,
      { "Code", "ambit.message.code", FT_UINT16, BASE_HEX,
        VALS(ambit_message_code_names), 0x0, NULL, HFILL } },
    { &hf_ambit_message_flags,
      { "Flags", "ambit.message.flags", FT_UINT8, BASE_HEX, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_message_status,
      { "Status", "ambit.message.status", FT_UINT8, BASE_HEX, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_message_format,
      { "Format", "ambit.message.format", FT_UINT16, BASE_HEX, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_message_counter,
      { "Counter", "ambit.message.counter", FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
    { &hf_ambit_message_size,
      { "Size", "ambit.message.size", FT_UINT32, BASE_DEC | BASE_UNIT_STRING,
        &units_byte_bytes, 0x0, NULL, HFILL } },
    { &hf_ambit_message_payload,
      { "Payload", "ambit.message.payload", FT_BYTES, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
    // Reassembly
    { &hf_ambit_message_fragments,
      { "Message fragments", "ambit.message.fragments", FT_NONE, BASE_NONE,
        NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_fragment,
      { "Message fragment", "ambit.message.fragment", FT_FRAMENUM, BASE_NONE,
        NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_fragment_overlap,
      { "Message fragment overlap", "ambit.message.fragment.overlap",
        FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_fragment_overlap_conflicts,
      { "Message fragment overlapping with conflicting data",
        "ambit.message.fragment.overlap.conflicts", FT_BOOLEAN, 0, NULL, 0x00,
        NULL, HFILL } },
    { &hf_ambit_message_fragment_multiple_tails,
      { "Message has multiple tail fragments",
        "ambit.message.fragment.multiple_tails", FT_BOOLEAN, 0, NULL, 0x00,
        NULL, HFILL } },
    { &hf_ambit_message_fragment_too_long_fragment,
      { "Message fragment too long", "ambit.message.fragment.too_long_fragment",
        FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_fragment_error,
      { "Message defragmentation error", "ambit.message.fragment.error",
        FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_fragment_count,
      { "Message fragment count", "ambit.message.fragment.count", FT_UINT32,
        BASE_DEC, NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_reassembled_in,
      { "Reassembled in", "ambit.message.reassembled.in", FT_FRAMENUM,
        BASE_NONE, NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_reassembled_length,
      { "Reassembled length", "ambit.message.reassembled.length", FT_UINT32,
        BASE_DEC, NULL, 0x00, NULL, HFILL } },
    { &hf_ambit_message_reassembled_data,
      { "Reassembled data", "ambit.message.reassembled.data", FT_BYTES,
        BASE_NONE, NULL, 0x00, NULL, HFILL } }
  };

  static gint* ett[] = { &ett_ambit_message, &ett_ambit_message_fragment,
                         &ett_ambit_message_fragments };

  proto_ambit_message =
    proto_register_protocol("Ambit Message", "Ambit Message", "ambit.message");

  proto_register_field_array(proto_ambit_message, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));

  ambit_payload_dissector_table =
    register_dissector_table("ambit.message.code", "Ambit Message Code",
                             proto_ambit_message, FT_UINT16, BASE_HEX);

  reassembly_table_register(&ambit_message_reassembly_table,
                            &addresses_reassembly_table_functions);

  static ei_register_info ei[] = {
    { &ei_ambit_message_undecoded,
      { "ambit.message.undecoded", PI_UNDECODED, PI_WARN,
        "Undecoded Message Bytes", EXPFILL } },
    { &ei_ambit_message_payload_undecoded,
      { "ambit.message.payload.undecoded", PI_UNDECODED, PI_WARN,
        "Undecoded Message Payload Bytes", EXPFILL } }
  };
  expert_module_t* expert_ambit_message;

  expert_ambit_message = expert_register_protocol(proto_ambit_message);
  expert_register_field_array(expert_ambit_message, ei, array_length(ei));
}

void
proto_reg_handoff__ambit_message(void)
{
  static dissector_handle_t ambit_message_handle;

  ambit_message_handle =
    create_dissector_handle(dissect_ambit_message, proto_ambit_message);
  dissector_add_uint("ambit.packet.marker", AMBIT_PACKET_MARKER,
                     ambit_message_handle);
}
