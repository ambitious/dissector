// data-block.c -- acquisition, `dd`-style
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <epan/packet.h>
#include <epan/unit_strings.h>

#include "ambit.h"

static int proto_ambit_data_block = -1;

static int hf_ambit_data_block_offset = -1;
static int hf_ambit_data_block_size = -1;
static int hf_ambit_data_block_data = -1;

static int
dissect_ambit_data_block_request(tvbuff_t* tvb, packet_info* pinfo,
                                 proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  ADD_ITEM(hf_ambit_data_block_offset, 4, ENC_LITTLE_ENDIAN);
  ADD_ITEM(hf_ambit_data_block_size, 4, ENC_LITTLE_ENDIAN);

  return offset;
}

static int
dissect_ambit_data_block_response(tvbuff_t* tvb, packet_info* pinfo,
                                  proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  ADD_ITEM(hf_ambit_data_block_offset, 4, ENC_LITTLE_ENDIAN);
  guint32 size = tvb_get_guint32(tvb, offset, ENC_LITTLE_ENDIAN);
  ADD_ITEM(hf_ambit_data_block_size, 4, ENC_LITTLE_ENDIAN);
  ADD_ITEM(hf_ambit_data_block_data, size, ENC_NA);

  return offset;
}

static int
dissect_ambit_data_block(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree,
                         void* data)
{
  ambit_message* message = (ambit_message*)data;

  return ((message->flags & 0x01)
            ? dissect_ambit_data_block_request(tvb, pinfo, tree, data)
            : dissect_ambit_data_block_response(tvb, pinfo, tree, data));
}

void
proto_register___ambit_data_block(void)
{
  static hf_register_info hf[] = {
    { &hf_ambit_data_block_offset,
      { "Offset", "ambit.data.block.offset", FT_UINT32,
        BASE_DEC | BASE_UNIT_STRING, &units_byte_bytes, 0x0, NULL, HFILL } },
    { &hf_ambit_data_block_size,
      { "Size", "ambit.data.block.size", FT_UINT32, BASE_DEC | BASE_UNIT_STRING,
        &units_byte_bytes, 0x0, NULL, HFILL } },
    { &hf_ambit_data_block_data,
      { "Block Data", "ambit.data.block.data", FT_BYTES, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
  };

  proto_ambit_data_block =
    proto_register_protocol("Data Block", "Data Block", "ambit.data.block");

  proto_register_field_array(proto_ambit_data_block, hf, array_length(hf));
}

void
proto_reg_handoff___ambit_data_block(void)
{
  static dissector_handle_t ambit_data_block_handle;

  ambit_data_block_handle =
    create_dissector_handle(dissect_ambit_data_block, proto_ambit_data_block);
  dissector_add_uint("ambit.message.code", 0x0700, ambit_data_block_handle);
  dissector_add_uint("ambit.message.code", 0x0b17, ambit_data_block_handle);
}
