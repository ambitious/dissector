// date-time.c -- syncing your watch's calendar and clock
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ambit.h"

static int proto_ambit_date_time = -1;

static int hf_ambit_date_time_date = -1;
static int hf_ambit_date_time_time = -1;

static int
dissect_ambit_date_time_request(tvbuff_t* tvb, packet_info* pinfo,
                                proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  ambit_tree_add_date(tree, hf_ambit_date_time_date, tvb, offset, 4);
  offset += 4;
  ambit_tree_add_time(tree, hf_ambit_date_time_time, tvb, offset, 4);
  offset += 4;

  return offset;
}

static int
dissect_ambit_date_time(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree,
                        void* data)
{
  ambit_message* message = (ambit_message*)data;

  return ((message->flags & 0x01)
            ? dissect_ambit_date_time_request(tvb, pinfo, tree, data)
            : 0);
}

void
proto_register___ambit_date_time(void)
{
  static hf_register_info hf[] = {
    { &hf_ambit_date_time_date,
      { "Date", "ambit.date_time.date", FT_STRING, BASE_NONE, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_date_time_time,
      { "Time", "ambit.date_time.time", FT_STRING, BASE_NONE, NULL, 0x0, NULL,
        HFILL } }
  };

  proto_ambit_date_time =
    proto_register_protocol("Set DateTime", "Set DateTime", "ambit.date_time");

  proto_register_field_array(proto_ambit_date_time, hf, array_length(hf));
}

void
proto_reg_handoff___ambit_date_time(void)
{
  static dissector_handle_t ambit_date_time_handle;

  ambit_date_time_handle =
    create_dissector_handle(dissect_ambit_date_time, proto_ambit_date_time);
  dissector_add_uint("ambit.message.code", 0x0300, ambit_date_time_handle);
}
