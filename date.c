// date.c -- syncing your watch's calendar
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ambit.h"

static int proto_ambit_date = -1;

static int hf_ambit_date_date = -1;

static int
dissect_ambit_date_request(tvbuff_t* tvb, packet_info* pinfo,
                           proto_tree* tree _U_, void* data _U_)
{
  gint offset = 0;

  ambit_tree_add_date(tree, hf_ambit_date_date, tvb, offset, 4);
  offset += 4;

  return offset;
}

static int
dissect_ambit_date(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree,
                   void* data)
{
  ambit_message* message = (ambit_message*)data;

  return ((message->flags & 0x01)
            ? dissect_ambit_date_request(tvb, pinfo, tree, data)
            : 0);
}

void
proto_register___ambit_date(void)
{
  static hf_register_info hf[] = { { &hf_ambit_date_date,
                                     { "Date", "ambit.date.date", FT_STRING,
                                       BASE_NONE, NULL, 0x0, NULL, HFILL } } };

  proto_ambit_date =
    proto_register_protocol("Set Date", "Set Date", "ambit.date");

  proto_register_field_array(proto_ambit_date, hf, array_length(hf));
}

void
proto_reg_handoff___ambit_date(void)
{
  static dissector_handle_t ambit_date_handle;

  ambit_date_handle =
    create_dissector_handle(dissect_ambit_date, proto_ambit_date);
  dissector_add_uint("ambit.message.code", 0x0302, ambit_date_handle);
}
