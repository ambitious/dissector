// ambit.c -- implement shared functionality
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ambit.h"

proto_item*
ambit_tree_add_date(proto_tree* tree, int hfindex, tvbuff_t* tvb,
                    const gint start, gint length)
{
  guint16 y = tvb_get_guint16(tvb, start, ENC_LITTLE_ENDIAN);
  guint8 m = tvb_get_guint8(tvb, start + 2);
  guint8 d = tvb_get_guint8(tvb, start + 3);

  return proto_tree_add_string_format_value(tree, hfindex, tvb, start, length,
                                            NULL, "%d-%02d-%02d", y, m, d);
}

proto_item*
ambit_tree_add_time(proto_tree* tree, int hfindex, tvbuff_t* tvb,
                    const gint start, gint length)
{
  guint8 h = tvb_get_guint8(tvb, start);
  guint8 m = tvb_get_guint8(tvb, start + 1);
  guint16 ms = tvb_get_guint16(tvb, start + 2, ENC_LITTLE_ENDIAN);

  return proto_tree_add_string_format_value(tree, hfindex, tvb, start, length,
                                            NULL, "%02d:%02d:%02d.%03d", h, m,
                                            ms / 1000, ms % 1000);
}

proto_item*
ambit_tree_add_version(proto_tree* tree, int hfindex, tvbuff_t* tvb,
                       const gint start, gint length)
{
  guint8 major = tvb_get_guint8(tvb, start);
  guint8 minor = tvb_get_guint8(tvb, start + 1);
  guint16 patch = tvb_get_guint16(tvb, start + 2, ENC_LITTLE_ENDIAN);

  return proto_tree_add_string_format_value(
    tree, hfindex, tvb, start, length, NULL, "%d.%d.%d", major, minor, patch);
}
