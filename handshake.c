// handshake.c -- establishing common ground
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <epan/packet.h>

#include "ambit.h"

static int proto_ambit_handshake = -1;

static int hf_ambit_handshake_komposti_version = -1;
static int hf_ambit_handshake_nickname = -1;
static int hf_ambit_handshake_serialno = -1;
static int hf_ambit_handshake_firmware_version = -1;
static int hf_ambit_handshake_hardware_version = -1;
static int hf_ambit_handshake_bootstrap_loader_version = -1;

static int
dissect_ambit_handshake_request(tvbuff_t* tvb, packet_info* pinfo,
                                proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  ambit_tree_add_version(tree, hf_ambit_handshake_komposti_version, tvb, offset,
                         4);
  offset += 4;

  return offset;
}

static int
dissect_ambit_handshake_response(tvbuff_t* tvb, packet_info* pinfo,
                                 proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  ADD_ITEM(hf_ambit_handshake_nickname, 16, ENC_NA);
  ADD_ITEM(hf_ambit_handshake_serialno, 16, ENC_NA);
  ambit_tree_add_version(tree, hf_ambit_handshake_firmware_version, tvb, offset,
                         4);
  offset += 4;
  ambit_tree_add_version(tree, hf_ambit_handshake_hardware_version, tvb, offset,
                         4);
  offset += 4;
  ambit_tree_add_version(tree, hf_ambit_handshake_bootstrap_loader_version, tvb,
                         offset, 4);
  offset += 4;

  return offset;
}

static int
dissect_ambit_handshake(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree,
                        void* data)
{
  ambit_message* message = (ambit_message*)data;

  return ((message->flags & 0x01)
            ? dissect_ambit_handshake_request(tvb, pinfo, tree, data)
            : dissect_ambit_handshake_response(tvb, pinfo, tree, data));
}

void
proto_register___ambit_handshake(void)
{
  static hf_register_info hf[] = {
    { &hf_ambit_handshake_komposti_version,
      { "Komposti Version", "ambit.handshake.komposti_version", FT_STRING,
        BASE_NONE, NULL, 0x0, NULL, HFILL } },
    { &hf_ambit_handshake_nickname,
      { "Model Nickname", "ambit.handshake.nickname", FT_STRING, BASE_NONE,
        NULL, 0x0, NULL, HFILL } },
    { &hf_ambit_handshake_serialno,
      { "Serial Number", "ambit.handshake.serialno", FT_STRING, BASE_NONE, NULL,
        0x0, NULL, HFILL } },
    { &hf_ambit_handshake_firmware_version,
      { "Firmware Version", "ambit.handshake.firmware_version", FT_STRING,
        BASE_NONE, NULL, 0x0, NULL, HFILL } },
    { &hf_ambit_handshake_hardware_version,
      { "Hardware Version", "ambit.handshake.hardware_version", FT_STRING,
        BASE_NONE, NULL, 0x0, NULL, HFILL } },
    { &hf_ambit_handshake_bootstrap_loader_version,
      { "Bootstrap Loader Version", "ambit.handshake.bootstrap_loader_version",
        FT_STRING, BASE_NONE, NULL, 0x0, NULL, HFILL } }
  };

  proto_ambit_handshake =
    proto_register_protocol("Handshake", "Handshake", "ambit.handshake");

  proto_register_field_array(proto_ambit_handshake, hf, array_length(hf));
}

void
proto_reg_handoff___ambit_handshake(void)
{
  static dissector_handle_t ambit_handshake_handle;

  ambit_handshake_handle =
    create_dissector_handle(dissect_ambit_handshake, proto_ambit_handshake);
  dissector_add_uint("ambit.message.code", 0x0000, ambit_handshake_handle);
  dissector_add_uint("ambit.message.code", 0x0002, ambit_handshake_handle);
}
