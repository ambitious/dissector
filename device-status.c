// device-status.c --
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ambit.h"

#include <epan/expert.h>
#include <epan/unit_strings.h>

static int proto_ambit_device_status = -1;

static int hf_ambit_device_status_charge = -1;

extern expert_field ei_ambit_message_payload_undecoded;

static int
dissect_ambit_device_status_reply(tvbuff_t* tvb, packet_info* pinfo,
                                  proto_tree* tree, void* data _U_)
{
  gint offset = 0;

  proto_tree_add_expert(tree, pinfo, &ei_ambit_message_payload_undecoded, tvb,
                        offset, 1);
  offset += 1;
  ADD_ITEM(hf_ambit_device_status_charge, 1, ENC_NA);
  return offset;
}

static int
dissect_ambit_device_status(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree,
                            void* data)
{
  ambit_message* message = (ambit_message*)data;

  return ((message->flags & 0x01) ? 0 : dissect_ambit_device_status_reply(
                                          tvb, pinfo, tree, data));
}

void
proto_register___ambit_device_status(void)
{
  static hf_register_info hf[] = { { &hf_ambit_device_status_charge,
                                     { "Battery Charge", "ambit.status.charge",
                                       FT_UINT8, BASE_DEC | BASE_UNIT_STRING,
                                       &units_percent, 0x0, NULL, HFILL } } };

  proto_ambit_device_status = proto_register_protocol(
    "Get Device Status", "Get Device Status", "ambit.status");

  proto_register_field_array(proto_ambit_device_status, hf, array_length(hf));
}

void
proto_reg_handoff___ambit_device_status(void)
{
  static dissector_handle_t ambit_device_status_handle;

  ambit_device_status_handle = create_dissector_handle(
    dissect_ambit_device_status, proto_ambit_device_status);
  dissector_add_uint("ambit.message.code", 0x0306, ambit_device_status_handle);
}
