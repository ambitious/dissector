// packet.c -- header dissection and message dissector hand-off
// Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <epan/dissectors/packet-usb.h>
#include <epan/unit_strings.h>
#include <wsutil/crc16.h>

#include "ambit.h"

int proto_ambit_packet = -1;

static int hf_ambit_packet_marker = -1;
static int hf_ambit_packet_offset = -1;
static int hf_ambit_packet_type = -1;
static int hf_ambit_packet_size = -1;
static int hf_ambit_packet_index = -1;
static int hf_ambit_packet_crc1 = -1;
static int hf_ambit_packet_crc1_status = -1;
static int hf_ambit_packet_payload = -1;
static int hf_ambit_packet_crc2 = -1;
static int hf_ambit_packet_padding = -1;
static int hf_ambit_packet_crc2_status = -1;

static gint ett_ambit_packet = -1;

static dissector_table_t ambit_message_dissector_table;

static const value_string ambit_packet_type_names[] = {
  { AMBIT_STARTER_PACKET, "starter packet" },
  { AMBIT_TRAILER_PACKET, "trailer packet" },
  { 0, NULL }
};

static guint16
crc16_x25_ccitt_tvb_offset_seed(tvbuff_t* tvb, guint offset, guint len,
                                guint16 seed)
{
  const guint8* buf;

  tvb_ensure_bytes_exist(tvb, offset, len);
  buf = tvb_get_ptr(tvb, offset, len);

  return crc16_x25_ccitt_seed(buf, len, seed);
}

static int
dissect_ambit_packet(tvbuff_t* tvb, packet_info* pinfo, proto_tree* _tree,
                     void* data _U_)
{
  if (tvb_get_guint8(tvb, 0) != AMBIT_PACKET_MARKER)
    return 0;

  col_set_str(pinfo->cinfo, COL_PROTOCOL, "Ambit");

  proto_item* tree_item =
    proto_tree_add_item(_tree, proto_ambit_packet, tvb, 0, -1, ENC_NA);
  proto_tree* tree = proto_item_add_subtree(tree_item, ett_ambit_packet);

  ambit_packet packet;

  gint offset = 0;
  ADD_ITEM(hf_ambit_packet_marker, 1, ENC_NA);
  ADD_ITEM(hf_ambit_packet_offset, 1, ENC_NA);
  packet.type = tvb_get_guint8(tvb, offset);
  ADD_ITEM(hf_ambit_packet_type, 1, ENC_NA);

  packet.size = tvb_get_guint8(tvb, offset);
  ADD_ITEM(hf_ambit_packet_size, 1, ENC_NA);
  packet.index = tvb_get_guint16(tvb, offset, ENC_LITTLE_ENDIAN);
  ADD_ITEM(hf_ambit_packet_index, 2, ENC_LITTLE_ENDIAN);

  guint16 crc1 = crc16_x25_ccitt_tvb_offset_seed(tvb, 2, 4, 0xffff);
  proto_tree_add_checksum(tree, tvb, offset, hf_ambit_packet_crc1,
                          hf_ambit_packet_crc1_status, NULL, pinfo, crc1,
                          ENC_LITTLE_ENDIAN, PROTO_CHECKSUM_VERIFY);
  offset += 2;
  ADD_ITEM(hf_ambit_packet_payload, packet.size, ENC_NA);

  guint16 crc2 = crc16_x25_ccitt_tvb_offset_seed(tvb, 8, packet.size, crc1);
  proto_tree_add_checksum(tree, tvb, offset, hf_ambit_packet_crc2,
                          hf_ambit_packet_crc2_status, NULL, pinfo, crc2,
                          ENC_LITTLE_ENDIAN, PROTO_CHECKSUM_VERIFY);
  offset += 2;

  if (offset < tvb_captured_length(tvb))
    ADD_ITEM(hf_ambit_packet_padding, -1, ENC_NA);

  if (packet.size > 0) {
    tvbuff_t* next_tvb = tvb_new_subset_length(tvb, 8, packet.size);
    dissector_try_uint_new(ambit_message_dissector_table, AMBIT_PACKET_MARKER,
                           next_tvb, pinfo, tree, FALSE, &packet);
  }
  return tvb_captured_length(tvb);
}

void
proto_register_ambit(void)
{
  static hf_register_info hf[] = {
    { &hf_ambit_packet_marker,
      { "Marker", "ambit.packet.marker", FT_UINT8, BASE_HEX, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_packet_offset,
      { "Offset", "ambit.packet.offset", FT_UINT8, BASE_DEC, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_packet_type,
      { "Type", "ambit.packet.type", FT_UINT8, BASE_HEX,
        VALS(ambit_packet_type_names), 0x0, NULL, HFILL } },
    { &hf_ambit_packet_size,
      { "Size", "ambit.packet.size", FT_UINT8, BASE_DEC | BASE_UNIT_STRING,
        &units_byte_bytes, 0x0, NULL, HFILL } },
    { &hf_ambit_packet_index,
      { "Index", "ambit.packet.index", FT_UINT16, BASE_DEC, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_packet_crc1,
      { "CRC-1", "ambit.packet.crc1", FT_UINT16, BASE_HEX, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_packet_crc1_status,
      { "CRC-1 Status", "ambit.packet.crc1.status", FT_UINT8, BASE_NONE,
        VALS(proto_checksum_vals), 0x0, NULL, HFILL } },
    { &hf_ambit_packet_payload,
      { "Payload", "ambit.packet.payload", FT_BYTES, BASE_NONE, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_packet_crc2,
      { "CRC-2", "ambit.packet.crc2", FT_UINT16, BASE_HEX, NULL, 0x0, NULL,
        HFILL } },
    { &hf_ambit_packet_crc2_status,
      { "CRC-2 Status", "ambit.packet.crc2.status", FT_UINT8, BASE_NONE,
        VALS(proto_checksum_vals), 0x0, NULL, HFILL } },
    { &hf_ambit_packet_padding,
      { "Padding", "ambit.packet.padding", FT_BYTES, BASE_NONE, NULL, 0x0, NULL,
        HFILL } },
  };
  static gint* ett[] = { &ett_ambit_packet };

  proto_ambit_packet =
    proto_register_protocol("Ambit Packet", "Ambit Packet", "ambit");

  proto_register_field_array(proto_ambit_packet, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));

  ambit_message_dissector_table =
    register_dissector_table("ambit.packet.marker", "Ambit Packet Marker",
                             proto_ambit_packet, FT_UINT8, BASE_HEX);
}

void
proto_reg_handoff_ambit(void)
{
  static dissector_handle_t ambit_packet_handle;

  ambit_packet_handle =
    create_dissector_handle(dissect_ambit_packet, proto_ambit_packet);
  dissector_add_uint("usb.interrupt", IF_CLASS_HID, ambit_packet_handle);
  dissector_add_uint("usb.interrupt", IF_CLASS_UNKNOWN, ambit_packet_handle);
}
